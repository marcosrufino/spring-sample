FROM frolvlad/alpine-java:jdk8-slim
        VOLUME /tmp
        ADD  target/spring-boot-hello-world-1.0-SNAPSHOT.jar search.jar
        EXPOSE 8090
        ENTRYPOINT ["java","-jar","/search.jar"]
